apiVersion: v1
kind: Secret
metadata:
  name: basic-auth
  namespace: webdav
type: Opaque
stringData:
  auth: "{{ .Values.basic_auth }}"
