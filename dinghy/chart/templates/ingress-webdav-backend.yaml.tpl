apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    ingress-waf/additional-crs: |
      SecAction "id:900200,phase:1,nolog,pass,t:none,setvar:\'tx.allowed_methods=GET HEAD POST PUT OPTIONS DELETE PATCH\'"
      SecRuleRemoveById 920340 920420
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/auth-realm: Authentication Required
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/rewrite-target: /$1
  name: webdav-backend
  namespace: webdav
spec:
  rules:
    - host: "{{ .Values.domain }}"
      http:
        paths:
          - backend:
              service:
                name: backend
                port:
                  number: 8080
            path: /_/(.+)
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - "{{ .Values.domain }}"
      secretName: "{{ .Values.tlsSecretName }}"
