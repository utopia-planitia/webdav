apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/auth-realm: Authentication Required
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-type: basic
  name: webdav
  namespace: webdav
spec:
  rules:
    - host: "{{ .Values.domain }}"
      http:
        paths:
          - backend:
              service:
                name: frontend
                port:
                  number: 8000
            path: /
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - "{{ .Values.domain }}"
      secretName: "{{ .Values.tlsSecretName }}"
