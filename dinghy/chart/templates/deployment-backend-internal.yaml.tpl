apiVersion: apps/v1
kind: Deployment
metadata:
  name: backend-internal
spec:
  replicas: 2
  selector:
    matchLabels:
      app: backend-internal
  template:
    metadata:
      labels:
        app: backend-internal
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchExpressions:
                  - key: app
                    operator: In
                    values:
                      - backend-internal
              topologyKey: "kubernetes.io/hostname"
      containers:
        - name: backend
          image: ghcr.io/damoon/dinghy/backend:latest@sha256:f9e656f50ba5d66b625d67053a7239f39cb8d1476d257158d4eaa7018e038087
          args:
            - --s3-endpoint=rook-ceph-rgw-generic.rook-ceph.svc:80
            - --s3-access-key-file=/secret/access_key
            - --s3-secret-key-file=/secret/secret_key
            - --s3-ssl=false
            - --s3-bucket=dinghy-storage
            - --frontend-url=https://{{ .Values.domain }}
          env:
            - name: JAEGER_AGENT_HOST
              value: jaeger.monitoring.svc
            - name: JAEGER_AGENT_PORT
              value: "6831"
          ports:
            - name: service
              containerPort: 8080
            - name: admin
              containerPort: 8090
          readinessProbe:
            httpGet:
              path: /healthz
              port: 8090
          volumeMounts:
            - name: secret
              mountPath: "/secret"
              readOnly: true
      volumes:
        - name: secret
          secret:
            secretName: dinghy-storage-bucket
            items:
            - key: AWS_ACCESS_KEY_ID
              path: access_key
            - key: AWS_SECRET_ACCESS_KEY
              path: secret_key
